package troposphere

import (
	"bufio"
	"bytes"
	"fmt"
)

type Stat struct {
	Name    string
	IsDir   bool
	Size    int64
	Updated int64
}

func (s *Stat) Bytes() []byte {
	b := new(bytes.Buffer)
	b.WriteString(fmt.Sprintf("%s\n", s.Name))
	b.WriteString(fmt.Sprintf("%v\n", s.IsDir))
	b.WriteString(fmt.Sprintf("%v\n", s.Size))
	b.WriteString(fmt.Sprintf("%v\n", s.Updated))
	return b.Bytes()
}

func ReadStat(r *bufio.Reader) (Stat, error) {
	s := Stat{}
	var err error
	if s.Name, err = readString(r); err != nil {
		return s, err
	}
	if s.IsDir, err = readBool(r); err != nil {
		return s, err
	}
	if s.Size, err = readInt64(r); err != nil {
		return s, err
	}
	if s.Updated, err = readInt64(r); err != nil {
		return s, err
	}
	return s, nil
}
