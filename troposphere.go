package troposphere

import (
	"bufio"
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"errors"
	"fmt"
	"gitlab.com/pbohun/troposphere/security"
	"io"
	"strconv"
	"strings"
)

const (
	Tversion = iota
	Tauth
	Tread
	Twrite
	Tremove
	Texec
	Tmkdir
	Tstat
	Rversion
	Rauth
	Rread
	Rwrite
	Rremove
	Rexec
	Rmkdir
	Rstat
	Rerror
)

var kindNames = []string{
	"Tversion",
	"Tauth",
	"Tread",
	"Twrite",
	"Tremove",
	"Texec",
	"Tmkdir",
	"Tstat",
	"Rversion",
	"Tauth",
	"Rread",
	"Rwrite",
	"Rremove",
	"Rexec",
	"Rmkdir",
	"Rstat",
	"Rerror",
}

func KindInt(s string) int {
	for i, k := range kindNames {
		if s == k {
			return i
		}
	}
	return -1
}

func KindString(k int) string {
	if k < 0 || k > len(kindNames) {
		return ""
	}
	return kindNames[k]
}

type Message struct {
	Kind      string
	Uid       string
	Path      string
	Pubkey    *rsa.PublicKey
	Offset    int64
	Length    int64
	Content   []byte
	Signature string
}

func (m *Message) String() string {
	pubkey := ""
	if m.Pubkey != nil {
		pubkey = security.ExportPublicKey(m.Pubkey)
	}
	content := base64.StdEncoding.EncodeToString(m.Content)
	if pubkey != "" {
		pubkey = base64.StdEncoding.EncodeToString([]byte(pubkey))
	}
	siglen := 344
	if len(m.Signature) == 0 {
		siglen = 0
	}
	// NOTE: +38 is for the length of the offset and length, and +8 is for the newlines
	// we don't count the newline after size because that's part of the first line
	size := len(m.Kind) + len(m.Path) + len(pubkey) + 38 + len(content) + siglen + 8
	sb := new(strings.Builder)
	sb.WriteString(fmt.Sprintf("%v\n", size))
	sb.WriteString(fmt.Sprintf("%s\n", m.Kind))
	sb.WriteString(fmt.Sprintf("%s\n", m.Uid))
	sb.WriteString(fmt.Sprintf("%s\n", m.Path))
	sb.WriteString(fmt.Sprintf("%s\n", pubkey))
	sb.WriteString(fmt.Sprintf("%19d\n", m.Offset))
	sb.WriteString(fmt.Sprintf("%19d\n", m.Length))
	sb.WriteString(fmt.Sprintf("%s\n", content))
	if len(m.Signature) == 0 {
		sb.WriteString("\n")
	} else {
		sb.WriteString(fmt.Sprintf("%344s\n", m.Signature))
	}
	return sb.String()
}

func (m *Message) KindInt() int {
	return KindInt(m.Kind)
}

func (m *Message) SignatureContent() []byte {
	pubkey := security.ExportPublicKey(m.Pubkey)
	pubkey = base64.StdEncoding.EncodeToString([]byte(pubkey))
	offset := fmt.Sprintf("%19d\n", m.Offset)
	length := fmt.Sprintf("%19d\n", m.Length)
	content := base64.StdEncoding.EncodeToString(m.Content)
	size := len(m.Kind) + len(m.Uid) + len(m.Path) + len(pubkey) + len(offset) + len(length) + len(content) + 4
	sizeStr := strconv.FormatInt(int64(size), 10)
	return []byte(sizeStr + m.Kind + m.Uid + m.Path + pubkey + content)
}

func (m *Message) Sign(privKey *rsa.PrivateKey) error {
	if privKey == nil {
		return nil
	}
	signature, err := security.HashAndSign(m.SignatureContent(), privKey)
	if err != nil {
		return err
	}
	m.Signature = base64.StdEncoding.EncodeToString(signature)
	return nil
}

func (m *Message) Verify() error {
	if m.Signature == "" {
		return nil
	}
	hash, err := security.Hash(m.SignatureContent())
	if err != nil {
		return err
	}
	signature, err := base64.StdEncoding.DecodeString(m.Signature)
	if err != nil {
		return err
	}
	return security.VerifySignature(hash, signature, m.Pubkey)
}

func (m *Message) Encrypt(pubKey *rsa.PublicKey) error {
	ciphertext, err := security.RsaEncrypt(m.Content, pubKey)
	if err != nil {
		return err
	}
	m.Content = ciphertext
	return nil
}

func (m *Message) Decrypt(privKey *rsa.PrivateKey) error {
	plaintext, err := security.RsaDecrypt(m.Content, privKey)
	if err != nil {
		return err
	}
	m.Content = plaintext
	return nil
}

func ReadMessage(r *bufio.Reader, maxSize int) (Message, error) {
	m := Message{}
	var err error
	size := 0
	if size, err = readInt(r); err != nil {
		return m, err
	}
	if size > maxSize {
		return m, errors.New(fmt.Sprintf("Message is (%v) bytes, maximum is (%v) bytes", size, maxSize))
	}
	buf := make([]byte, size)
	n, err := io.ReadFull(r, buf)
	if err != nil {
		return m, err
	}
	if n != size {
		return m, errors.New(fmt.Sprintf("Got (%v) bytes, expected (%v) bytes", n, size))
	}
	rd := bufio.NewReader(bytes.NewReader(buf))
	if m.Kind, err = readString(rd); err != nil {
		return m, err
	}
	if m.Uid, err = readString(rd); err != nil {
		return m, err
	}
	if m.Path, err = readString(rd); err != nil {
		return m, err
	}
	pkey, err := readString(rd)
	if err != nil {
		return m, err
	}
	m.Offset, err = readInt64(rd)
	if err != nil {
		return m, err
	}
	m.Length, err = readInt64(rd)
	if err != nil {
		return m, err
	}
	if pkey != "" {
		pkeybytes, err := base64.StdEncoding.DecodeString(pkey)
		if err != nil {
			return m, err
		}
		pubkey, err := security.ImportPublicKey(string(pkeybytes))
		if err != nil {
			return m, err
		}
		m.Pubkey = pubkey
	}
	s, err := readString(rd)
	if err != nil {
		return m, err
	}
	content, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return m, err
	}
	m.Content = content
	if m.Signature, err = readString(rd); err != nil && err != io.EOF {
		return m, err
	}
	err = m.Verify()
	return m, err
}

func readBool(r *bufio.Reader) (bool, error) {
	s, err := r.ReadString('\n')
	if err != nil && err != io.EOF {
		return false, err
	}
	s = strings.TrimSpace(s)
	res := false
	if s == "true" {
		res = true
	}
	return res, nil
}

func readString(r *bufio.Reader) (string, error) {
	s, err := r.ReadString('\n')
	if err != nil && err != io.EOF {
		return "", err
	}
	return strings.TrimSpace(s), nil
}

func readInt(r *bufio.Reader) (int, error) {
	line, err := r.ReadString('\n')
	if err != nil {
		return 0, err
	}
	line = strings.TrimSpace(line)
	n, err := strconv.ParseInt(line, 10, 64)
	return int(n), err
}

func readInt64(r *bufio.Reader) (int64, error) {
	line, err := r.ReadString('\n')
	if err != nil {
		return 0, err
	}
	line = strings.TrimSpace(line)
	n, err := strconv.ParseInt(line, 10, 64)
	return n, err
}

func MkMessage(kind int, path string, myPrivKey *rsa.PrivateKey, offset, length int64, content []byte) (Message, error) {
	var myPubKey *rsa.PublicKey
	if myPrivKey != nil {
		myPubKey = &myPrivKey.PublicKey
	}
	if len(content) > 0 {
		length = int64(len(content))
	}
	m := Message{
		Kind:    KindString(kind),
		Path:    path,
		Pubkey:  myPubKey,
		Offset:  offset,
		Length:  length,
		Content: content,
	}
	if err := m.Sign(myPrivKey); err != nil {
		return m, err
	}
	return m, nil
}
