#!/usr/bin/sh
mkdir bin

go build -o bin/00-message/message examples/00-message/main.go

go build -o bin/01-time/server examples/01-time/server.go
go build -o bin/01-time/client examples/01-time/client.go

go build -o bin/02-add/server examples/02-add/server.go
go build -o bin/02-add/client examples/02-add/client.go

go build -o bin/03-files/server examples/03-files/server.go
go build -o bin/03-files/client examples/03-files/client.go

