package troposphere

import (
	"bufio"
	"strings"
	"testing"
)

const pubkeyFname string = "./tests/pubkey"
const privkeyFname string = "./tests/privkey"

const statString string = `test
false
1234
1699919664471
`
const messageString string = `971
Tread

/example
LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1JSUJDZ0tDQVFFQTVZaVc1bkF5aG5saXZKNFZFVzF1QmQ2d2szMEhFandtck42Z1lGQ0g4bHNUbEFEb0dpMm0KMHdTNTNsTEhzU2JQVFRQd21uTTNiU0VJeXQ3MWJmeG1ZMEhwc001dlgybTlOdTBjY1g1MExUazlzOGVRWitoUApldWtWbGFTRGlqc3AyU2hSTUNtWWRURjZLTHdERldLL2d0QUJ1akx6ekN6ZlZrSEtSajE1anRzQ1I3cXpSTGtECnRHZUt0aDNsemNtM25BcHF5ODk3bmFMbEZDRFhBNjczVEZEVGVqOVNidDhBRktORU5Tc0psN2NpUmdoVmFlVlUKU1ZOVlJtODBWTldHekJVaU95Q1N5SHY5Q1dxT212R2hUVVlzZkpBbEp1QThYeHBIa0lJUTV2UEh2bTg3L2dZTgphZEFSNCtlZmI0L1E4cHc2am9HbHhhUjhqRGhib2hvRTBRSURBUUFCCi0tLS0tRU5EIFJTQSBQVUJMSUMgS0VZLS0tLS0K
                  0
                  0

zYlq8TB1Pih+tbByghYHPKHEvrWzB1ZCLiwyx0yonIqh3Jmx1Irl9Y9xUhCS1boalgpTYclxOQcCj0jaYyNiauAv8V8iRPORjxXVSLwj8aSEucGhSJWNKW73jkISNcBIBC8LSAZ6qvG6gy21+ahA3zfIa1t8PZTTLmcYJ9T7NnaZ2J7hVHFhgRO0md6fbEmj4F9/7YGdzsm1jFG7QXLzmJ7OPoAaHROnqI0ifTLh7YEYB8vAJx6/83GkEGYPV81s6qgJWTIbkQWFkhDWt9LhPzbDK7o/EbJCkV1SKASHOZoUSA0p6xFwb9Yi8EqYIIIgN/9FHvAE41FPNrGLZe4z8Q==
`

func TestReadStat(t *testing.T) {
	r := bufio.NewReader(strings.NewReader(statString))
	stat, err := ReadStat(r)
	if err != nil {
		t.Fatal(err.Error())
	}
	afterRead := string(stat.Bytes())
	if afterRead != statString {
		t.Fatalf("Stat doesn't match. Got:\n%s\nwanted:\n%s\n", afterRead, statString)
	}
}

func TestReadMessage(t *testing.T) {
	r := bufio.NewReader(strings.NewReader(messageString))
	msg, err := ReadMessage(r, 1024)
	if err != nil {
		t.Fatal(err.Error())
	}
	msgStr := msg.String()
	if msgStr != messageString {
		t.Fatalf("Message doesn't match. Got:\n%s\nwanted:\n%s\n", msgStr, messageString)
	}
}
