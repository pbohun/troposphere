package security

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
)

const rsaKeySize = 2048

var nonce = []byte("012345678912")

func GenerateId() (string, error) {
	id := make([]byte, 10)
	_, err := rand.Read(id)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(id), nil
}

func GenerateKeypair() (*rsa.PrivateKey, error) {
	return rsa.GenerateKey(rand.Reader, rsaKeySize)
}

func RsaEncrypt(plaintext []byte, pubKey *rsa.PublicKey) ([]byte, error) {
	return rsa.EncryptOAEP(sha256.New(), rand.Reader, pubKey, plaintext, nil)
}

func RsaDecrypt(ciphertext []byte, privKey *rsa.PrivateKey) ([]byte, error) {
	return rsa.DecryptOAEP(sha256.New(), nil, privKey, ciphertext, nil)
}

func Hash(message []byte) ([]byte, error) {
	hasher := sha256.New()
	_, err := hasher.Write(message)
	if err != nil {
		return []byte{}, err
	}
	return hasher.Sum(nil), nil
}

func HashAndSign(message []byte, privKey *rsa.PrivateKey) ([]byte, error) {
	hashedMessage, err := Hash(message)
	if err != nil {
		fmt.Println("error hashing message:", err)
		return []byte{}, err
	}
	return rsa.SignPSS(rand.Reader, privKey, crypto.SHA256, hashedMessage, nil)
}

func VerifySignature(hashedMessage, signature []byte, pubKey *rsa.PublicKey) error {
	return rsa.VerifyPSS(pubKey, crypto.SHA256, hashedMessage, signature, nil)
}

func ExportPublicKey(pubKey *rsa.PublicKey) string {
	return string(pem.EncodeToMemory(&pem.Block{Type: "RSA PUBLIC KEY", Bytes: x509.MarshalPKCS1PublicKey(pubKey)}))
}

func ExportPrivateKey(privKey *rsa.PrivateKey) string {
	return string(pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)}))
}

func ImportPublicKey(key string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(key))
	return x509.ParsePKCS1PublicKey(block.Bytes)
}

func ImportPrivateKey(key string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(key))
	return x509.ParsePKCS1PrivateKey(block.Bytes)
}
