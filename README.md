# Troposphere
"Combine all networks into a single file system."

## Definition
Troposphere is a protocol designed to provide a uniform interface for web service and resource discovery.

## Example Implementation
An example implementation in Go has been provided in this repository.
It also contains example programs using the library.

### Build Requirements
- Go (version 1.21.4 or greater)
- shell (optional to run the build scripts)

## Examples
In addition to this documentation, the best resource for usage is in the `examples` directory.

## Building examples
Run the following script to build all the examples.
Make sure that the script file has permissions to run.
```
./build-examples.sh
```
The build script will create a `bin` directory and place all compiled examples there.

## Problem
Every web service API is different. 
This makes it nearly impossible to list, filter, explore, and compose webservices together. 
Imagine being able to do `$ cat inputs.json | web-service-a | web-service-b > outputs.json`.
Troposphere provides a uniform interface for web services that would make this possible.

## Solution
Troposphere is designed to provide a uniform interface to services and resources that is similar to the file system interface used locally on computers. 

## Description
Troposphere consists of 17 different message kinds that describe 8 different actions. 
Those actions are:

- Version
- Auth
- Read
- Write
- Remove
- Exec
- Mkdir
- Stat

Each action has 2 message kinds; a Transmission message, and a Response message.
Additionally, there is an Response message for errors.

Every Transmission message is responded to with a Response message, which could possibly be the Error message.

## Message Format
All messages are text-based and have the same fields.
This means that all payload data that is sent is first base64 encoded, regardless of being binary or not.
Each type of message consists of a list of fields.
These fields have a specified order, and each field is written on a separate line.
All numeric values are represented in string form. 

Message Field List:
```
<msg-size>
<kind>
<auth>
<pubkey>
<offset>
<length>
<content>
<signature>
```

The `msg-size` field is the number of bytes following the first line of the message to the end of the message.
The `kind` field is a string that labels the message kind.
The `auth` field is a string that is the authentication token provided by the service to the client.
The `pubkey` field is an RSA 2048 bit public key used to identify users and sign messages.
The `offset` field is a number represented as a 19 character string that has one of two meanings: 1) the number of bytes from the beginning of the file to read or write, or 2) the number of entries from the beginning of a directory to read. This number is left padded by spaces. This string is not null terminated.
The `length` field is a number represented as a 19 character string that has one of two meanings: 1) the number of bytes to read or write to a file, or 2) the maximum number of entries to read from a directory. This number is left padded by spaces. The value -1 indicates unlimited number of bytes or entries. This string is not null terminated.
The `content` field is a base64 encoding of the content, regardless of whether the content is a string or binary.
The `signature` field is a base64 encoding of the sha256 hashed string of `<kind> + <pubkey> + <offset> + <length> + <content>` that has been signed with the `pubkey`. The signature is always 344 bytes long and left padded with spaces as necessary.

Example of a Tread message:
```
970
Tread

/example
LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1JSUJDZ0tDQVFFQXJJckFKUTF2eU9KS1JNcHJiWXFOdGVUdjU3dU5yb3ZMTHcxNFFaTzVXTHU4V3ZYVlUyaHcKNW5BaFRjOU1JVkFlRENxbEp1U1JRSSs5WEI0RE8yY2Rid0hadld6Q0xBZWlHSzU3TXNIS2VRQzBVZmJSV0V6MQo0MUxPQ0NMZXlsYmErUUJMSnB0UEdWSGdKQ2JYcXNWK1d3bWZJSFZFZDhOZjBWTFZiL1hxRFY1bkRnK1JyOXMzCjBJcE43R21hZDVnNWUvYkVtVlNGK1pHSjdCYXA1aFdFeFAwSXlzaW5MVHZvQUdkZ3IyWFpqbXU2VU5YK3dZUU8Kbk1rOUtoWURPUXlYWDNkVG5nNExSdkR5NUZhT1owdjBwNGpDN05CL0VYenc5M01Zb21aVml0SmtqZmRyRlJKYwpOTC9pUDZwRWpBVjRiRjBuUDhJNTBmRkxvZmRUcFRwWEp3SURBUUFCCi0tLS0tRU5EIFJTQSBQVUJMSUMgS0VZLS0tLS0K
                  0
                  0

GgA87VWK2ilOJ5+Y52ERjKytJRNnkTre2x2Frl6wirBK/JOfbcgAidDOBcz5y3dRoC+QdfZLXBqbDkv7sVHVlQ0kuPZkCG3HO5zdT1bEGQR2BapZyDSUo0ggC6nH1ZEzKk931E8j2nImewKVZdxCRSukG6koTeiJ0rW0IrJwYyWdOGy9z+nKAkUG6bwQi4PUMZxQR8CrB2qJmGeiGME2Xas4iMRAEI2I00F+P9ZpQd5uv52Jm8tiRpju+o22RDsNL6blIMzAka/rz3H1K0dZ7CR+iT2tkVTEFhaz2W7x3nKnZcaiWdhKIGY1G3h6ouKQ2F5SuQ4zbnELEhliiELHzw==
```
Note that since there is no `content` for a `Tversion` message, there is an empty line for that field.

## Version Numbers
There are currently only two valid responses to a Tversion message.
They are "1.0" and "1.1". 
Version "1.0" indicates that the `pubkey` and `signature` fields can be blank.
This allows older computers that do not have the performance to do cryptographic functions to still use the protocol. 
Version "1.1" indicates that `pubkey` and `signature` fields are required. 
The `pubkey` is a 2048 bit RSA public key, and the signature is a SHA256 hash of the `size`, `kind`, `path`, `pubkey`, `offset`, `length`, and `content` fields signed by the corresponding private RSA key.

## Stat Data Structure
The stat data structure describes metadata about a resource, which can be file-like or folder-like.
Like the `Message` data structure, `Stat` fields are written as strings, one per line.
The fields are:
```
<name>
<isdir>
<size>
<updated>
```

`name` is a simple string.
`isdir` is a string representation of a boolean `true` or `false`.
`size` and `updated` are string representations of 64 bit integers.

### IsDir
`isdir` is a boolean value that indicates whether the entry is a directory.

### Size
`size` is a 64 bit integer that indicates the size of the resource in bytes.
A value of -1 indicates an indeterminate size or that size is irrelevant to the resource.

### Updated
`updated` is the last modification time of the resource.
This is a 64 bit integer milliseconds Unix timestamp.

## Protocol
A session has the following interaction timeline.

| Client   | Server   |
| -------- | -------- |
| Tversion |          |
|          | Rversion |
| (t-msg)  |          |
|          | (r-msg)  |

Note that `(t-msg)` refers to any Transmission message and `(r-msg)` refers to any Response message.

## Tests
There are just a couple tests to make sure that Stat and Message serialize and deserialize correctly.
To run the tests use the following command:
```
go test
```

## Unofficial Theme Song
`https://www.youtube.com/watch?v=dP4t_GGl3Es`

## License
3-Clause BSD License
