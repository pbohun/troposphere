package main

import (
	"bufio"
	"fmt"
	"strings"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

// This is a simple example of creating an decoding a troposphere message

func main() {
	privKey, _ := security.GenerateKeypair()

	m, _ := tro.MkMessage(tro.Tread, "/example", privKey, 0, 0, []byte{})
	mstr := m.String()
	fmt.Println("Message string:")
	fmt.Println(mstr)
	m2, err := tro.ReadMessage(bufio.NewReader(strings.NewReader(mstr)), 4096)
	if err != nil {
		fmt.Println(err)
		return
	}
	if messageEqual(m, m2) {
		fmt.Println("read message matches original")
	} else {
		fmt.Println("read message doesn't match original")
	}
}

func messageEqual(a, b tro.Message) bool {
	if a.Kind != b.Kind {
		fmt.Println("kind not equal:", a.Kind, ":", b.Kind)
		return false
	}
	if a.Path != b.Path {
		fmt.Println("path not equal:", a.Path, b.Path)
		return false
	}
	if a.Pubkey.Equal(*b.Pubkey) {
		fmt.Println("pubkey not equal:", a.Pubkey, b.Pubkey)
		return false
	}
	if a.Offset != b.Offset {
		fmt.Println("offset not equal:", a.Offset, b.Offset)
		return false
	}
	if a.Length != b.Length {
		fmt.Println("length not equal:", a.Length, b.Length)
		return false
	}
	if a.Signature != b.Signature {
		fmt.Println("signature not equal:")
		fmt.Println("sig a:", a.Signature)
		fmt.Println("sig b:", b.Signature)
		return false
	}
	if len(a.Content) != len(b.Content) {
		fmt.Println("content length doesn't match:", len(a.Content), len(b.Content))
		return false
	}
	for i, _ := range a.Content {
		if a.Content[i] != b.Content[i] {
			fmt.Println("content doesn't match at :", i, a.Content[i], b.Content[i])
			return false
		}
	}
	return true
}
