package main

import (
	"bufio"
	"crypto/rsa"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

const (
	maxSize = 4096
)

var privKey *rsa.PrivateKey

func main() {
	privKey, _ = security.GenerateKeypair()
	http.HandleFunc("/time", timeHandler)
	fmt.Println("Time service running at port 8080")
	http.ListenAndServe(":8080", nil)
}

func timeHandler(w http.ResponseWriter, r *http.Request) {
	// We only want the server to process a couple requests,
	// so have the program exit after a reasonable amount of time
	go func() {
		time.Sleep(time.Second * 1)
		os.Exit(0)
	}()

	// read the client message
	m, err := tro.ReadMessage(bufio.NewReader(r.Body), maxSize)
	if err != nil {
		sendError(w, "Could not read message")
		return
	}

	// we are only handling the /time path
	if m.Path != "/time" {
		sendError(w, "Expected /time path, got:"+m.Path)
		return
	}

	// handle the different message kinds
	switch m.KindInt() {
	case tro.Tversion:
		resp, _ := tro.MkMessage(tro.Rversion, m.Path, privKey, 0, 0, []byte("1.1"))
		io.WriteString(w, resp.String())
		return
	case tro.Tread:
		t := time.Now()
		resp, _ := tro.MkMessage(tro.Rread, m.Path, privKey, 0, 0, []byte(t.String()))
		io.WriteString(w, resp.String())
		return
	}

	// we should tell the user if they sent a message with an unsupported kind
	sendError(w, "Unsupported message kind:"+m.Kind)
}

func sendError(w http.ResponseWriter, msg string) {
	resp, _ := tro.MkMessage(tro.Rerror, "/time", privKey, 0, 0, []byte(msg))
	io.WriteString(w, resp.String())
}
