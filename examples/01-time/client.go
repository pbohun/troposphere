package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"strings"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
	privKey, _ := security.GenerateKeypair()
	url := "http://localhost:8080/time"

	// get server version
	req, _ := tro.MkMessage(tro.Tversion, "/time", privKey, 0, 0, []byte{})
	resp := makeRequest(url, req)
	fmt.Println("server version:", string(resp.Content))

	// ask for time
	req, _ = tro.MkMessage(tro.Tread, "/time", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	fmt.Println("server time is:", string(resp.Content))
	fmt.Println("success!")
}

func makeRequest(url string, msg tro.Message) tro.Message {
	resp, err := http.Post(url, "text/plain", strings.NewReader(msg.String()))
	if err != nil {
		log.Fatal(err)
	}
	rmsg, err := tro.ReadMessage(bufio.NewReader(resp.Body), 4096)
	if err != nil {
		log.Fatal(err)
	}
	return rmsg
}

