package main

import (
	"bufio"
	"crypto/rsa"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

const (
	maxSize = 4096
)

var privKey *rsa.PrivateKey

const filesDir string = "./files/"

func main() {
	os.MkdirAll(filesDir, 0777)
	privKey, _ = security.GenerateKeypair()
	http.HandleFunc("/files", filesHandler)
	fmt.Println("Time service running at port 8080")
	http.ListenAndServe(":8080", nil)
}

func filesHandler(w http.ResponseWriter, r *http.Request) {
	// We only want the server to process a couple requests,
	// so have the program exit after a reasonable amount of time
	go func() {
		time.Sleep(time.Second * 1)
		os.RemoveAll(filesDir)
		os.Exit(0)
	}()

	// read the client message
	m, err := tro.ReadMessage(bufio.NewReader(r.Body), maxSize)
	if err != nil {
		sendError(w, "Could not read message")
		return
	}

	// we are only handling the /files path
	if !strings.HasPrefix(m.Path, "/files") {
		sendError(w, "Expected /files path, got:"+m.Path)
		return
	}

	// set the real path
	path := strings.ReplaceAll(m.Path, "/files/", filesDir)

	// handle the different message kinds
	switch m.KindInt() {
	case tro.Tversion:
		resp, _ := tro.MkMessage(tro.Rversion, m.Path, privKey, 0, 0, []byte("1.1"))
		io.WriteString(w, resp.String())
		return
	case tro.Tread:
		info, err := os.Lstat(path)
		if err != nil {
			sendError(w, "File/directory does not exist")
			return
		}
		if info.IsDir() {
			files, _ := os.ReadDir(path)
			list := []string{}
			for _, f := range files {
				list = append(list, f.Name())
			}
			blist := []byte(strings.Join(list, "\n"))
			resp, _ := tro.MkMessage(tro.Rread, m.Path, privKey, 0, 0, blist)
			io.WriteString(w, resp.String())
			return
		}
		fileContents, _ := os.ReadFile(path)
		resp, _ := tro.MkMessage(tro.Rread, m.Path, privKey, 0, 0, fileContents)
		io.WriteString(w, resp.String())
		return
	case tro.Twrite:
		if os.WriteFile(path, m.Content, 0666) != nil {
			sendError(w, "Could not write file")
			return
		}
		bytesWritten := fmt.Sprintf("%v", len(m.Content))
		resp, _ := tro.MkMessage(tro.Rwrite, m.Path, privKey, 0, 0, []byte(bytesWritten))
		io.WriteString(w, resp.String())
		return
	case tro.Tremove:
		if os.RemoveAll(path) != nil {
			sendError(w, "Could not remove")
			return
		}
		resp, _ := tro.MkMessage(tro.Rremove, m.Path, privKey, 0, 0, []byte{})
		io.WriteString(w, resp.String())
		return
	case tro.Tmkdir:
		if err := os.MkdirAll(path, 0777); err != nil {
			fmt.Println("could not make dir:", err)
			sendError(w, "Could not make directory")
			return
		}
		resp, _ := tro.MkMessage(tro.Rmkdir, m.Path, privKey, 0, 0, []byte{})
		io.WriteString(w, resp.String())
		return
	case tro.Tstat:
		info, err := os.Lstat(path)
		if err != nil {
			sendError(w, "Could not get file stats")
			return
		}
		stat := tro.Stat{
			Name:        info.Name(),
			Size:        info.Size(),
			Updated:     info.ModTime().UnixMilli(),
		}
		resp, _ := tro.MkMessage(tro.Rstat, m.Path, privKey, 0, 0, stat.Bytes())
		io.WriteString(w, resp.String())
		return
	}

	// we should tell the user if they sent a message with an unsupported kind
	sendError(w, "Unsupported message kind:"+m.Kind)
}

func sendError(w http.ResponseWriter, msg string) {
	resp, _ := tro.MkMessage(tro.Rerror, "/files", privKey, 0, 0, []byte(msg))
	io.WriteString(w, resp.String())
}

