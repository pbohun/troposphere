package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"strings"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

func main() {
	privKey, _ := security.GenerateKeypair()
	url := "http://localhost:8080/files"

	// get server version
	req, _ := tro.MkMessage(tro.Tversion, "/files", privKey, 0, 0, []byte{})
	resp := makeRequest(url, req)
	fmt.Println("server version:", string(resp.Content))

	// mkdir
	req, _ = tro.MkMessage(tro.Tmkdir, "/files/mydir", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Println("made directory")
	}

	// write file
	content := []byte("This is my file")
	req, _ = tro.MkMessage(tro.Twrite, "/files/mydir/myfile.txt", privKey, 0, 0, content)
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Println("wrote file")
	}

	// read dir
	req, _ = tro.MkMessage(tro.Tread, "/files/mydir", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Println("directory contents:", string(resp.Content))
	}

	// read file
	req, _ = tro.MkMessage(tro.Tread, "/files/mydir/myfile.txt", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Println("got file:", string(resp.Content))
	}

	// get file stats
	req, _ = tro.MkMessage(tro.Tstat, "/files/mydir/myfile.txt", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Printf("file stats:\n%s", string(resp.Content))
	}

	// remove dir
	req, _ = tro.MkMessage(tro.Tremove, "/files/mydir", privKey, 0, 0, []byte{})
	resp = makeRequest(url, req)
	if resp.KindInt() == tro.Rerror {
		fmt.Println("error:", string(resp.Content))
	} else {
		fmt.Println("removed directory")
	}

	fmt.Println("success!")
}

func makeRequest(url string, msg tro.Message) tro.Message {
	resp, err := http.Post(url, "text/plain", strings.NewReader(msg.String()))
	if err != nil {
		log.Fatal(err)
	}
	rmsg, err := tro.ReadMessage(bufio.NewReader(resp.Body), 4096)
	if err != nil {
		log.Fatal(err)
	}
	return rmsg
}

