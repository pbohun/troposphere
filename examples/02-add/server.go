package main

import (
	"bufio"
	"crypto/rsa"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	tro "gitlab.com/pbohun/troposphere"
	"gitlab.com/pbohun/troposphere/security"
)

const (
	maxSize = 4096
)

var privKey *rsa.PrivateKey

func main() {
	privKey, _ = security.GenerateKeypair()
	http.HandleFunc("/add", addHandler)
	fmt.Println("Add service running at port 8080")
	http.ListenAndServe(":8080", nil)
}

func addHandler(w http.ResponseWriter, r *http.Request) {
	// We only want the server to process a couple requests,
	// so have the program exit after a reasonable amount of time
	go func() {
		time.Sleep(time.Second * 1)
		os.Exit(0)
	}()

	// read the client message
	m, err := tro.ReadMessage(bufio.NewReader(r.Body), maxSize)
	if err != nil {
		sendError(w, "Could not read message")
		return
	}

	// we are only handling the /add path
	if m.Path != "/add" {
		sendError(w, "Expected /add path, got:"+m.Path)
		return
	}

	// handle the different possible message kinds
	switch m.KindInt() {
	case tro.Tversion:
		resp, _ := tro.MkMessage(tro.Rversion, m.Path, privKey, 0, 0, []byte("1.1"))
		io.WriteString(w, resp.String())
		return
	case tro.Texec:
		lines := strings.Split(string(m.Content), "\n")
		if len(lines) != 2 {
			sendError(w, fmt.Sprintf("Got %d lines instead of 2", len(lines)))
			return
		}
		a, err := strconv.ParseInt(lines[0], 10, 64)
		if err != nil {
			sendError(w, fmt.Sprintf("Invalid integer:%s", lines[0]))
			return
		}
		b, err := strconv.ParseInt(lines[1], 10, 64)
		if err != nil {
			sendError(w, fmt.Sprintf("Invalid integer:%s", lines[1]))
			return
		}
		answer := fmt.Sprintf("%d", a+b)
		resp, _ := tro.MkMessage(tro.Rexec, m.Path, privKey, 0, 0, []byte(answer))
		io.WriteString(w, resp.String())
		return
	}

	// we should tell the user if they sent a message with an unsupported kind
	sendError(w, "Unsupported message kind:"+m.Kind)
}

func sendError(w http.ResponseWriter, msg string) {
	resp, _ := tro.MkMessage(tro.Rerror, "/add", privKey, 0, 0, []byte(msg))
	io.WriteString(w, resp.String())
}
